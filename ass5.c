#include <stdio.h>
#include <string.h>
#include <conio.h>
#include <stdlib.h>
#include <windows.h>

int findAge(int bday, int bmonth, int byear, int jday, int jmonth, int jyear)
{
    int age;
    if (bmonth < jmonth)
        age = jyear - byear;
    else if (bmonth > jmonth)
        age = jyear - byear - 1;
    else
    {
        if (bday <= jday)
            age = jyear - byear;
        else
            age = jyear - byear;
    }
    //printf("\n%d/%d/%d",bday,bmonth,byear);
    //printf("\n%d/%d/%d",jday,jmonth,jyear);
    return age;
}

int findExperience(int jday, int jmonth, int jyear)
{
    int exp;
    //Get the curren date
    SYSTEMTIME stime;
    GetSystemTime(&stime);
    //calculate experience
    if (jyear != stime.wYear)
    {
        if (jmonth == stime.wMonth)
        {
            if (jday <= stime.wDay)
                exp = (int)(stime.wYear - jyear) * 12;
            else
                exp = (int)(stime.wYear - jyear) * 12 - 1;
        }
        else if (jmonth < stime.wMonth)
        {
            if (jday <= stime.wDay)
                exp = (stime.wYear - jyear) * 12 + (stime.wMonth - jmonth);
            else
                exp = (stime.wYear - jyear) * 12 + (stime.wMonth - jmonth) - 1;
        }
        else
        {
            if (jday <= stime.wDay)
                exp = (stime.wYear - jyear) * 12 - (stime.wMonth - jmonth);
            else
                exp = (stime.wYear - jyear) * 12 - (stime.wMonth - jmonth) - 1;
        }
    }
    else
    {
        if (jmonth == stime.wMonth)
            exp = 0;
        else if (jmonth < stime.wMonth)
        {
            if (jday <= stime.wDay)
                exp = (stime.wMonth - jmonth);
            else
                exp = (stime.wMonth - jmonth) - 1;
        }
        else
        {
            printf("Invalid Joining Date!!");
            return -1;
        }
    }
    return exp;
}
int isLeap(int year)
{
    return (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0));
}
int *findProbationPeriod(int jday, int jmonth, int jyear, int pperiod[])
{
    int i, day = jday, month = jmonth, year = jyear;
    int flag = 0;
    int months[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    for (i = 1; i <= 3; i++)
    {
        if (month > 12)
        {
            month = month % 12;
            flag++;
        }
        day = day + 30;
        if (day > months[month - 1])
        {
            day = day - months[month - 1];
            month++;
        }
    }
    if (isLeap(jyear))
        day -= 2;
    else
        day--;
    if (flag != 0)
        year++;
    pperiod[0] = day;
    pperiod[1] = month;
    pperiod[2] = year;
    return pperiod;
}

int main()
{
    setvbuf(stdout, NULL, _IONBF, 0);
    int empId;
    char name[50], address[100], dateOfBirth[20], dateOfJoining[20];
    float salary;
    int bday, bmonth, byear;
    int jday, jmonth, jyear;
    int pperiod[3];
    printf("Enter Employee ID:");
    scanf("%d", &empId);
    printf("Enter Salary:");
    scanf("%f", &salary);
    getchar();
    printf("Enter Name:");
    gets(name);
    printf("Enter Address:");
    gets(address);
    //getchar();
    printf("Enter Date of Birth (dd/mm/yyyy):");
    gets(dateOfBirth);
    //getchar();
    printf("Enter Date of Joining (dd/mm/yyyy):");
    gets(dateOfJoining);

    //Convert dateofbirth string to integer
    bday = atoi(strtok(dateOfBirth, "/"));
    bmonth = atoi(strtok(NULL, "/"));
    byear = atoi(strtok(NULL, "/"));

    //Convert dateofjoining string to integer
    jday = atoi(strtok(dateOfJoining, "/"));
    jmonth = atoi(strtok(NULL, "/"));
    jyear = atoi(strtok(NULL, "/"));

    printf("\nYour Age at the time of joining the company: %d", findAge(bday, bmonth, byear, jday, jmonth, jyear));
    printf("\nYou have a total experience of %d months", findExperience(jday, jmonth, jyear));
    findProbationPeriod(jday, jmonth, jyear, pperiod);
    printf("\nYour Probation Period ends on: %d/%d/%d", pperiod[0], pperiod[1], pperiod[2]);
    return 0;
}