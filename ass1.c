#include <stdio.h>
#include <string.h>
int main(int argc, char *argv[])
{
    if (argc == 1)
    {
        printf("Please pass any argument through command line!!!");
    }
    else
    {
        char *values = strtok(argv[1], ",");
        while (values != NULL)
        {
            printf("%s\n", values);
            values = strtok(NULL, ",");
        }
    }
    return 0;
}