#include <stdio.h>
#include <stdlib.h>
#define size 10
#define size1 50

int strcomparison(char *str, char *str1)
{
    int i = 0;
    int flag = 0;
    while (flag == 0)
    {
        if (str[i] > str1[i])
        {
            flag = 1;
        }
        else if (str[i] < str1[i])
        {
            flag = -1;
        }

        if (str[i] == '\0')
        {
            break;
        }

        i++;
    }
    return flag;
}
void strreverse(char *str)
{
    int length, j, i = 0;
    ;
    char rev[size];
    while (str[i] != '\0')
    {
        i++;
    }

    length = i - 1;

    for (j = 0; j < i; j++)
    {
        rev[j] = str[length];
        length--;
    }

    printf("%s", rev);
}
void strcopy(char *dest, char *src)
{
    int i = 0;

    for (i = 0; i < size; i++)
    {
        dest[i] = src[i];
    }
    printf("Copied String is:  %s", dest);
}
void stringcat(char *str, char *str1)
{
    int i = 0, j = 0;
    char temp[size1];
    while (str[i] != '\0')
    {
        temp[j] = str[i];
        i++;
        j++;
    }

    i = 0;

    while (str1[i] != '\0')
    {
        temp[j] = str1[i];
        i++;
        j++;
    }

    temp[j] = '\0';

    printf("\n%s", temp);
}

int main()
{
    char str[size], str1[size], choice;
    int result;
    while (1)
    {

        printf("\n---Menu---");
        printf("\n1.String Copy");
        printf("\n2.String Comparison");
        printf("\n3.String Concatenation");
        printf("\n4.String Reverse");
        printf("\n5.Exit");
        printf("\nEnter your choice: ");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:

            printf("Enter Source string: ");
            scanf("%s", str);
            printf("Enter Destination string: ");
            scanf("%s", str1);
            strcopy(str1, str);
            break;

        case 2:

            printf("Enter First string: ");
            scanf("%s", str);
            printf("Enter Second string: ");
            scanf("%s", str1);
            result = strcomparison(str, str1);
            printf("%d", result);
            break;

        case 3:

            printf("Enter First string: ");
            scanf("%s", str);
            printf("Enter Second string: ");
            scanf("%s", str1);
            stringcat(str, str1);
            break;

        case 4:

            printf("Enter string: ");
            scanf("%s", str);
            strreverse(str);
            break;

        case 5:

            exit(1);
            break;

        default:
            printf("Wrong Input");
        }
    }
    return 0;
}
