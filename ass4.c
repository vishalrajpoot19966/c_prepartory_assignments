#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct book
{
    int id;
    char name[100];
    float price;
} Book;

void merge(Book arr[], int first, int mid, int last)
{
    Book sortedArr[10];
    int i, j, k;
    i = first;
    j = mid + 1;
    k = first;

    while (i <= mid && j <= last)
    {
        if (arr[i].price > arr[j].price)
            sortedArr[k++] = arr[i++];
        else
            sortedArr[k++] = arr[j++];
    }
    for (; i <= mid; i++)
        sortedArr[k++] = arr[i];
    for (; j <= last; j++)
        sortedArr[k++] = arr[j];
    for (i = first; i <= last; i++)
        arr[i] = sortedArr[i];
}

void mergeSort(Book arr[], int first, int last)
{
    int mid;
    if (first < last)
    {
        mid = (first + last) / 2;
        mergeSort(arr, first, mid);
        mergeSort(arr, mid + 1, last);
        merge(arr, first, mid, last);
    }
}

int comparator(const void *p, const void *q)
{
    return strcmp(((struct book *)p)->name, ((struct book *)q)->name);
}

int main()
{
    int i;
    Book arr[10] = {{100, "The Alchemist", 199},
                    {200, "The Monk Who Sold His Ferrari", 156},
                    {300, "The Subtle Art of Not Giving a F*ck", 298},
                    {400, "Chanakya Neeti", 123},
                    {500, "Think and Grow Rich", 220},
                    {600, "Start With Why", 425},
                    {700, "Zero to One", 170},
                    {800, "All the Light we Cannot See", 228},
                    {900, "The Five AM Club", 214},
                    {1000, "The Four Hour Workweek", 350}};
    mergeSort(arr, 0, 9);
    printf("Array after mergesort:\n");
    for (i = 0; i < 10; i++)
    {
        printf("%d,%s,%f\n", arr[i].id, arr[i].name, arr[i].price);
    }
    printf("-------------------------------------------------------------");
    qsort(arr, 10, sizeof(struct book), comparator);
    printf("\nsorted books by name using qsort library function are: \n\n");
    for (i = 0; i < 10; i++)
        printf("%d, %s, %d\n", arr[i].id, arr[i].name, arr[i].price);
    printf("\n");

    return 0;
}