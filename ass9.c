#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct item 
{
   int id,quantity;
   char name[20];
   float price;
}Item;

enum operations
{
    EXIT,ADD,FIND,DISPLAYALL,EDIT,DELETE
};

int get_next_item_id() {
    FILE*fp;
    int max=0;
    Item i;
    long size=sizeof(Item);
    fp=fopen("item.db","rb");
    if(fp==NULL)
        return max+1;
    fseek(fp,-size,SEEK_END);
    while(fread(&i,sizeof(Item),1,fp)>0){
        max=i.id;
    }
    fclose(fp);
    return max+1;
}
void add() {
    FILE *fp;
    Item item;
    item.id=get_next_item_id();
    fp=fopen("item.db","ab");
    printf("Enter Item Details:\n");
    printf("Name:");
    getchar();
    scanf("%s",item.name);
    printf("Quantity:");
    scanf("%d",&item.quantity);
    printf("Price:");
    scanf("%f",&item.price);
    fwrite(&item,sizeof(Item),1,fp); //Youtube Reference-fwrite( ) and fread( ) to write and read structure from file using file handling in c programming
    fclose(fp);
}
void find_by_name() {
    FILE* fp;
    char name[10];
    Item i;
    int found=0;
    fp=fopen("item.db","rb");
    if(fp==NULL){
        perror("Cannot open item file.\n");
        return;
    }
    printf("Enter item name: ");
    getchar();
    gets(name);
    while(fread(&i,sizeof(Item),1,fp)>0){
        if(strcmp(i.name,name)==0){
            found=1;
            printf("Item Details: %d %s %d %f\n",i.id,i.name,i.quantity,i.price);
            break;
        }
    }
    if(found==0)
        printf("Item not found.\n");
    fclose(fp);
}

void edit_by_name() {
    FILE* fp;
    char name[10];
    Item i;
    int found=0;
    fp=fopen("item.db","rb+");
    if(fp==NULL){
        perror("Cannot open item file.\n");
        return;
    }
    printf("Enter item name: ");
    getchar();
    gets(name);
    while(fread(&i,sizeof(Item),1,fp)>0){
        if(strcmp(i.name,name)==0){
            found=1;
            printf("Name:");
            //getchar();
            scanf("%s",i.name);
            printf("Quantity:");
            scanf("%d",&i.quantity);
            printf("Price:");
            scanf("%f",&i.price);
            fseek(fp,-sizeof(Item),SEEK_CUR);
            fwrite(&i,sizeof(Item),1,fp);
            printf("Item edited successfully!!\n");
            //printf("Item Details: %d %s %d %f\n",i.id,i.name,i.quantity,i.price);
            break;
        }
    }
    if(found==0)
        printf("Item not found.\n");
    fclose(fp);
}
void displayAll() {
    FILE*fp;
    fp=fopen("item.db","rb");
    Item item;
    while(fread(&item,sizeof(Item),1,fp)>0)
        printf("%d, %s, %d, %f\n",item.id,item.name,item.quantity,item.price);
    fclose(fp);
}

void delete() {
    FILE* fp,*fptemp;
    char name[10];
    Item i;
    int found=0;
    fp=fopen("item.db","rb");
    if(fp==NULL){
        perror("Cannot open item file.\n");
        return;
    }
    fptemp=fopen("temp.db","ab");
    if(fp==NULL){
        perror("Cannot open temp file.\n");
        return;
    }
    printf("Enter name of the item to be deleted: ");
    getchar();
    gets(name);
    while(fread(&i,sizeof(Item),1,fp)>0){
        if(strcmp(i.name,name)!=0){
            fwrite(&i,sizeof(Item),1,fptemp);
        }
        else{
            found=1;
            printf("Item with name %s is deleted successfully!\n",i.name);
        }
    }
    if(found==0){
        printf("Item not found.\n");
        return;
    }
    fclose(fp);
    fclose(fptemp);
    remove("item.db");
    rename("temp.db","item.db");
}


int main() {
    int choice;
    FILE* fp;
    //fp=fopen("item.bin","wb+");
   // fpr=fopen("item.txt","rb");
    do{
        printf("\n\n0. Exit\n1. Add\n2. Find\n3. Display All\n4. Edit\n5. Delete\n");
        printf("Enter Choice:");
        scanf("%d",&choice);
        switch(choice)
        {
            case ADD:
                add();
                break;
            case FIND://find by name
                find_by_name();
                break;
            case DISPLAYALL:
                displayAll();
                break;
            case EDIT:
                edit_by_name();
                break;
            case DELETE:
                delete();
                break;
        }
    }while(choice!=0);
    fclose(fp);
    return 0;
}