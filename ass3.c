#include <stdio.h>
#include <stdlib.h>
int main()
{
    int arr[10];
    int choice, num, index, ind, index1;
    int max = arr[0];
    int min = arr[0];
    int maxIndex = 0;
    int minIndex = 0;
    while (1)
    {
        printf("1. Add number\n2. Delete number\n3. Maximum number\n4. Minimum number\n");
        printf("5. Sum of numbers\n6. Display the array\n7. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);
        switch (choice)
        {
        case 1:
            printf("Enter the number: ");
            scanf("%d", &num);
            printf("Available indexes:");
            ind = 0;
            for (int i = 0; i < 10; i++)
            {
                if (arr[ind] == 0)
                {
                    printf("%d ", ind);
                }
                ind++;
            }
            //ind=10
            printf("\nEnter the index where number is to be added: ");
            scanf("%d", &index);
            if (index < 0 && index >= 10)
            {
                printf("Please enter valid index\n");
            }
            else
            {
                arr[index] = num;
                printf("Number added successfully!!!\n");
            }
            break;
        case 2:
            ind = 0;
            printf("Available indexes:");
            for (int i = 0; i < 10; i++)
            {
                if (arr[ind] != 0)
                {
                    printf("index=%d & value=%d\n", ind, arr[ind]);
                }
                ind++;
            }
            printf("Enter the index:");
            scanf("%d", &index);
            printf("index=%d & value=%d deleted successfully\n", index, arr[index]);
            arr[index] = 0;
            break;
        case 3:
            max = arr[0];
            for (int i = 0; i < 10; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                    maxIndex = i;
                }
            }
            printf("Maximum number = %d at index = %d\n", max, maxIndex);
            break;
        case 4:
            min = arr[0];
            for (int i = 0; i < 10; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                    minIndex = i;
                }
            }
            printf("Minimum number = %d at index = %d\n", min, minIndex);
            break;
        case 5:
            ind = 0;
            printf("Available indexes:");
            for (int i = 0; i < 10; i++)
            {
                if (arr[ind] != 0)
                {
                    printf("index=%d & value=%d\n", ind, arr[ind]);
                }
                ind++;
            }
            printf("Enter the indexes which are to be added(addition):");
            scanf("%d %d", &index, &index1);
            printf("Sum of %d and %d is %d\n", arr[index], arr[index1], (arr[index] + arr[index1]));
            break;
        case 6:
            for (int i = 0; i < 10; i++)
            {
                printf("%d ", arr[i]);
            }
            printf("\n");
            break;
        case 7:
            exit(0);
        default:
            printf("Please enter valid choice!!!\n");
        }
    }
}